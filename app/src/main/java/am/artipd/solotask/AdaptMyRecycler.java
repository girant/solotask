package am.artipd.solotask;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static am.artipd.solotask.GlobalVariables.SET_API_URLS;
import static am.artipd.solotask.GlobalVariables.SET_NAME;
import static am.artipd.solotask.GlobalVariables.SPREFS_NAME;
import static am.artipd.solotask.GlobalVariables.inflaterLayoutId;

public class AdaptMyRecycler extends RecyclerView.Adapter<AdaptMyRecycler.MyAdaptersViewHolder> {

    List<DataItemObject> dataList;
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEdit;
    Set <String> stringSet, favoritesSet;
    ImageClickListener imageClickListener;
    StarClickListener starClickListener;


    public AdaptMyRecycler(List<DataItemObject> dataList, Context context, ImageClickListener imageClickListener, StarClickListener starClickListener) {
        this.dataList = dataList;
        this.imageClickListener = imageClickListener;
        this.context = context;
        this.starClickListener = starClickListener;
        sharedPreferences = context.getSharedPreferences(SPREFS_NAME, Context.MODE_PRIVATE);


        stringSet = new HashSet<>();
        favoritesSet = new HashSet<>();
        stringSet = sharedPreferences.getStringSet(SET_NAME, new HashSet<>());
        favoritesSet = sharedPreferences.getStringSet(SET_API_URLS, new HashSet<>());
        inflaterLayoutId = sharedPreferences.getBoolean("viewV2", false) ? R.layout.to_inflate_to_list_v2 : R.layout.to_inflate_to_list;

    }
    @NonNull
    @Override
    public MyAdaptersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("SOLOAAdaptMyRecycler", "onCreateViewHolder called");

        return new MyAdaptersViewHolder(LayoutInflater.from(parent.getContext()).inflate(inflaterLayoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdaptersViewHolder holder, int position) {
        Log.d("SOLOAAdaptMyRecycler", "BindViewHolder  called");


        DataItemObject objectAtPosition = dataList.get(position);
        holder.tvTitle.setText(objectAtPosition.articleTitle);
        holder.tvCategory.setText(objectAtPosition.articleCategory);
        holder.ivArticlePic.setImageBitmap(objectAtPosition.newsImage);
        holder.btnFav.setChecked(objectAtPosition.isChecked);
        View vItem = holder.itemView.findViewById(R.id.lLayCard);
        vItem.setOnClickListener(v->{
            imageClickListener.onImageClick(dataList.get(position), holder.ivArticlePic, position);
            //Intent intent = new Intent(context.getApplicationContext(), OpenNewsPageActivity.class);
            //intent.putExtra("newsUrl", dataList.get(position).articleURL);
            //context.startActivity(intent);
            Log.d("SOLOAppAdaptMyRecycl",String.valueOf( objectAtPosition.articleURL));
        });

        View vFav = holder.itemView.findViewById(R.id.btnFavorite);
        vFav.setOnClickListener(v->{
            if (holder.btnFav.isChecked()){
                stringSet.add(objectAtPosition.newsId);
                favoritesSet.add(objectAtPosition.apiUrl);
                objectAtPosition.isChecked = true;
                Log.d("SOLOAppAdaptMyRecycl", "Item added to set with id " + dataList.get(position).newsId);
            }
            else {
                stringSet.remove(objectAtPosition.newsId);
                favoritesSet.remove(objectAtPosition.apiUrl);
                objectAtPosition.isChecked = false;
                Log.d("SOLOAppAdaptMyRecycl", "Item deleted from set with id " + dataList.get(position).newsId);
            }
            spEdit = sharedPreferences.edit();
            spEdit.remove(SET_NAME);
            spEdit.remove(SET_API_URLS);
            spEdit.apply();

            spEdit.putStringSet(SET_NAME, stringSet);
            spEdit.putStringSet(SET_API_URLS, favoritesSet);
            spEdit.apply();
            starClickListener.onStarClick(position, objectAtPosition.isChecked);
            Log.d("SOLOAppAdaptMyRecycl", "Changes are applied to prefs");

        });


    }

    @Override
    public int getItemCount() {
        Log.d("SOLOAAdaptMyRecycler", "getItem called, count: " + (dataList.size()));

        return dataList.size();
    }

    public class MyAdaptersViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvCategory;
        ImageView ivArticlePic;
        CheckBox btnFav;
        public MyAdaptersViewHolder(@NonNull View itemView) {
            super(itemView);

            Log.d("SOLOAAdaptMyRecycler", "ViewHolder called");

            tvTitle = itemView.findViewById(R.id.tvArticleTitle);
            tvCategory =itemView.findViewById(R.id.tvArticleCategory);
            ivArticlePic = itemView.findViewById(R.id.ivNewsImage);
            btnFav = itemView.findViewById(R.id.btnFavorite);
        }
    }

}
