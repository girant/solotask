package am.artipd.solotask;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static am.artipd.solotask.GlobalVariables.API_KEY;
import static am.artipd.solotask.GlobalVariables.EXTRA_API_URL;
import static am.artipd.solotask.GlobalVariables.EXTRA_ID;
import static am.artipd.solotask.GlobalVariables.EXTRA_NEWS_URL;
import static am.artipd.solotask.GlobalVariables.MAIN_URL_WITH_API_KEY;
import static am.artipd.solotask.GlobalVariables.SET_API_URLS;
import static am.artipd.solotask.GlobalVariables.SET_NAME;
import static am.artipd.solotask.GlobalVariables.SPREFS_NAME;
import static am.artipd.solotask.GlobalVariables.addedToFavList;
import static am.artipd.solotask.GlobalVariables.inflaterLayoutId;
import static am.artipd.solotask.GlobalVariables.isActivityLaunched;
import static am.artipd.solotask.GlobalVariables.lastNewsDate;
import static am.artipd.solotask.GlobalVariables.lastNewsDateLong;
import static am.artipd.solotask.GlobalVariables.viewMode;
import static am.artipd.solotask.RequestAndResponse.getConnection;
import static am.artipd.solotask.RequestAndResponse.getJsonString;

public class MainActivity extends AppCompatActivity implements ImageClickListener, StarClickListener {
    private RecyclerView recyclerView;
    private AdaptMyRecycler recAdapter;
    private LinearLayoutManager layoutManager;
    private List<DataItemObject> DIOList;
    private DataItemObject DIO;
    private DataItemObject DIONew = null;
    boolean isScrolling = false;
    boolean favCreated = false;  //to say dont update list, just favorite list creating finished
    ProgressBar pbLoad;
    boolean updateList = false;
    String jsonString;
    int pageNumber;
    private static MainActivity mainActivity;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEdit;
    boolean onFavorites = false;
    Set<String> prefsSet, favoritesSet;
    public static final int REQ_CODE = 1111;
    int scrollToPosition;

    public MainActivity(){
        mainActivity = this;
    }

    public static MainActivity getInstance(){
        return mainActivity;
    }

    public void refresh(DataItemObject DIONew){
        if (DIONew !=null){
            this.DIONew = DIONew;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get preferences
        sharedPreferences = getSharedPreferences(SPREFS_NAME, MODE_PRIVATE);
        inflaterLayoutId = sharedPreferences.getBoolean("viewV2", false) ? R.layout.to_inflate_to_list_v2 : R.layout.to_inflate_to_list;

        //Create sets
        prefsSet = new HashSet<>();
        favoritesSet = new HashSet<>();

        //Get sets
        prefsSet = sharedPreferences.getStringSet(SET_NAME, new HashSet<>());
        favoritesSet = sharedPreferences.getStringSet(SET_API_URLS, new HashSet<>());

        pageNumber = 2;

        recyclerView = findViewById(R.id.recView);
        DIOList = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recAdapter= new AdaptMyRecycler(DIOList, this, this::onImageClick, this::onStarClick);

        fillFavorites(); //Step 1   //on first start we will fill at first favorites list, then first page

        //Set adapter
        recyclerView.setAdapter(recAdapter);

        //attach ids to views
        pbLoad = findViewById(R.id.pbLoad);
        pbLoad.setVisibility(View.VISIBLE);

        //Start service to check news
        startService(new Intent(this, CheckAndNotifyService.class));

        //Add onScrollListener to retrive new data from server while scrolling to footer
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isScrolling = true;
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleCount = layoutManager.getChildCount();
                int itemTotalCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (visibleCount+firstVisibleItem)>=itemTotalCount){
                    isScrolling = false;
                    Log.d("SOLOAppMainActiv", "Loading");
                    pbLoad.setVisibility(View.VISIBLE);
                    fillData(String.valueOf(pageNumber));
                    pageNumber++;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Get sets to check if there any changes in the stored data
        favoritesSet = sharedPreferences.getStringSet(SET_API_URLS, new HashSet<>());
        prefsSet = sharedPreferences.getStringSet(SET_NAME, new HashSet<>());


        //Set activity state indicator to true
        isActivityLaunched = true;
        if (updateList || addedToFavList){
            Log.d("SOLOAppMainActiv", "Main onresume was called in IF");

        }
        //notify the adapter that data was changed
        recAdapter.notifyDataSetChanged();
        Log.d("SOLOAppMainActiv", "Main activity onresume was called");
    }

    //Add method to check the application is running or not
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        isActivityLaunched = false;
        Log.d("SOLOAppMainActiv", "OnUserLeave Called");
    }

    //Fill data into RecyclerView after favorites data list was created
    public void fillData(final String pageNumber)  {
        // create AsyncTask to create data list for adapter
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                Log.d("SOLOAppMainActiv", "AsyncTask doInBackground called");

                //getStringData from server
                jsonString = getJsonString(getConnection(MAIN_URL_WITH_API_KEY +  pageNumber));
                Log.d("SOLOAppMainActiv", jsonString);
                JSONObject jsonObject;
                JSONArray jsonArray;

                try {
                    //create json object from json string
                    jsonObject = new JSONObject(jsonString);
                    JSONObject jo = jsonObject.getJSONObject("response");
                    jsonArray = jo.getJSONArray("results");
                    // check if page number is 1 to get last news adding date
                    if(pageNumber.equals("1") ){
                        lastNewsDate = jsonArray.getJSONObject(0).getString("webPublicationDate");
                        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date date = simpleDateFormat.parse(lastNewsDate);
                        lastNewsDateLong = date.getTime()/1000;
                        Log.d("SOLOAppMainActiv", String.valueOf(lastNewsDateLong));
                    }
                    for (int i=0;i<jsonArray.length(); i++){
                        Document doc;
                        JSONObject itemObj = jsonArray.getJSONObject(i);

                        //get news page and put it into document, to catch the news image from it
                        doc = Jsoup.connect(itemObj.getString("webUrl")).timeout(0).get();

                        //Get image element from document
                        Element image = doc.getElementsByClass("maxed responsive-img").first();
                        String imageUrl;

                        //Get main image url
                        if (image!=null){
                            imageUrl = image.absUrl("src");
                        }
                        else {
                            imageUrl = "";
                        }

                        //Create bitmap from url
                        Bitmap imgBitmap;
                        if (!imageUrl.equals("")){
                            imgBitmap = Picasso.with(getApplicationContext()).load(imageUrl).get();
                        }
                        else{
                            imgBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.noimageavailb);
                        }

                        //if item is in preferences, then dont add the it to list
                            if (prefsSet.contains(itemObj.getString("id"))){
                                Log.d("SOLOAppMainActiv", itemObj.getString("id") + " is in prefs");
                            }
                            else {
                                Log.d("SOLOAppMainActiv", itemObj.getString("id") + " is not in prefs");
                                DIO = new DataItemObject(itemObj.getString("webTitle"),
                                        "Category: " + itemObj.getString("sectionName"),
                                        itemObj.getString("webUrl"), itemObj.getString("id"), itemObj.getString("apiUrl"), false, imgBitmap);

                                DIOList.add(DIO);

                            }


                    }
                    Log.d("SOLOAppMainActiv", "DIO LIST " +DIOList.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pbLoad.setVisibility(View.GONE);
                recAdapter.notifyDataSetChanged();
            }
        }.execute();
    }

    //fill favorite items list
    public void fillFavorites(){
        DIOList.clear();
        recAdapter.notifyDataSetChanged();
        if(favoritesSet.size()>0){
            //check, if favorites list is not empty, create async task to fill recycler with favorites
            //run AsyncTask to fill favorites
            new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    String favoriteItemJsonString;
                    JSONObject favObject;
                    JSONObject favObjectPure;


                    for (String apiUrlAtPosition : favoritesSet){
                        try {
                            favoriteItemJsonString = getJsonString(getConnection(apiUrlAtPosition + "?" + API_KEY));
                            favObject = new JSONObject(favoriteItemJsonString);
                            favObjectPure = favObject.getJSONObject("response").getJSONObject("content");
                            Document doc = Jsoup.connect(favObjectPure.getString("webUrl")).timeout(0).get();
                            Element image = doc.getElementsByClass("maxed responsive-img").first();
                            String imageUrl;
                            Bitmap imgBitmap;
                            if (image!=null){
                                imageUrl = image.absUrl("src");
                                imgBitmap = Picasso.with(getApplicationContext()).load(imageUrl).get();
                            }
                            else {
                                imgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.noimageavailb);
                            }

                            onFavorites = true;

                            DataItemObject DAOFAV = new DataItemObject(favObjectPure.getString("webTitle"),
                                    "Category: " + favObjectPure.getString("sectionName"),
                                    favObjectPure.getString("webUrl"), favObjectPure.getString("id"), favObjectPure.getString("apiUrl"), onFavorites, imgBitmap);
                            DIOList.add(DAOFAV);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    favCreated = true; //to remind that favorites list is alredy created
                    //fill list from retrived data from server
                    fillData("1");  //Step 2
                }
            }.execute();
        }
        else {
            fillData("1");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.getItem(1).setChecked(sharedPreferences.getBoolean("viewV2", false));
        viewMode = sharedPreferences.getBoolean("viewV2", false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuClearAll:
                spEdit = sharedPreferences.edit();
                spEdit.clear();
                updateList = true;
                //refresh();
                break;
            case R.id.app_bar_switch:
                spEdit = sharedPreferences.edit();
                item.setChecked(!viewMode);
                viewMode = item.isChecked();
                Log.d("SOLOAppMainActiv", "Checkbox state is " + item.isChecked());
                inflaterLayoutId = viewMode ? R.layout.to_inflate_to_list_v2 : R.layout.to_inflate_to_list;
                spEdit.putBoolean("viewV2", viewMode);
                Log.d("SOLOAppMainActiv", String.valueOf(item.isEnabled()
                ));
                Log.d("SOLOAppMainActiv", String.valueOf(viewMode));

                updateList = true;
                recAdapter.notifyItemRangeChanged(0, DIOList.size());
                recyclerView.setAdapter(recAdapter);

                break;
                default:break;

        }
        spEdit.apply();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isActivityLaunched = false;
        Log.d("SOLOAppMainActiv", "OnBackPressed was called");

    }

    @Override
    public void onImageClick(DataItemObject DIO, ImageView iv, int position) {
        //create intent to start
        Intent intent = new Intent(this, OpenNewsPageActivity.class);

        //Put extra into intent
        intent.putExtra(EXTRA_NEWS_URL, DIO.articleURL);
        intent.putExtra("bitmap", DIO.newsImage);
        intent.putExtra(EXTRA_ID, DIO.newsId);
        intent.putExtra(EXTRA_API_URL, DIO.apiUrl);

        scrollToPosition = position;

        //Starting activity for result with bundle
        ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, iv, "transit");
        startActivityForResult(intent, REQ_CODE, activityOptions.toBundle());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE){

            Log.d("SOLOAppMainActiv", "Activityresult CODE is " + resultCode);

            switch (resultCode){
                case 20:
                  addOrRemoveItemToPosition(scrollToPosition, 0, false, false);
                    Log.d("SOLOAppMainActiv", "Activityresult 20 true");
                    break;

                case 21:
                    addOrRemoveItemToPosition(scrollToPosition, 0, true,false);
                    break;

                case 10:
                    recyclerView.scrollToPosition(scrollToPosition);
                    Log.d("SOLOAppMainActiv", "Activityresult 10 true");
                    break;

                    default:break;

            }
        }
    }

    @Override
    public void onStarClick(int position, boolean ischecked) {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (ischecked){
            Log.d("SOLOAppMainActiv", "Selected position = " + position);

            addOrRemoveItemToPosition(position, 0,true, false);
            Log.d("SOLOAppMainActiv", "Selected position to add = " + position);
        }
        else {
            Log.d("SOLOAppMainActiv", "Selected position = " + position);

            addOrRemoveItemToPosition(position, 0, false, false);
            Log.d("SOLOAppMainActiv", "Selected position to remove = " + position);
        }

    }

    public void addOrRemoveItemToPosition(int positionFrom, int positionTo, boolean add, boolean newItem){
        favoritesSet = sharedPreferences.getStringSet(SET_API_URLS, new HashSet<>());
        prefsSet = sharedPreferences.getStringSet(SET_NAME, new HashSet<>());
        int prefsSize = prefsSet.size();
        Log.d("SOLOAppMainActiv", "Prefs size = " + prefsSize);


        DataItemObject DIOTemp = DIOList.get(positionFrom);
        DIOList.remove(positionFrom);
        if (add){
            DIOTemp.isChecked = true;
            DIOList.add(positionTo, DIOTemp);
            recAdapter.notifyItemMoved(positionFrom, positionTo);
            recyclerView.scrollToPosition(positionTo);
            recAdapter.notifyItemChanged(positionTo);
            recAdapter.notifyItemRangeChanged(0, positionFrom+1);
            Log.d("SOLOAppMainActiv", "From " + positionFrom + "TO " + positionTo +
                    "Prefs size= " + prefsSize);

        }
        else {
            DIOTemp.isChecked = false;
            DIOList.add(prefsSize, DIOTemp);
            recAdapter.notifyItemMoved(positionFrom, prefsSize);
            recyclerView.scrollToPosition(prefsSize);
            recAdapter.notifyItemRangeChanged(0, prefsSize+1);
            Log.d("SOLOAppMainActiv", "From " + positionFrom + " TO " + prefsSize +
                    "Prefs size= " + prefsSize);

        }
        if (newItem){
            DIOTemp = DIONew;
            DIOList.add(prefsSize, DIOTemp);
            recAdapter.notifyItemInserted(prefsSize);
            recyclerView.scrollToPosition(prefsSize);
            recAdapter.notifyItemChanged(prefsSize);
            Log.d("SOLOAppMainActiv", "From " + positionFrom + "TO " + positionTo +
                    "Prefs size= " + prefsSize);
        }
        recAdapter.notifyItemRangeChanged(0, scrollToPosition);
    }
}
