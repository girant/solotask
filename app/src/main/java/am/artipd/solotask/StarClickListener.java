package am.artipd.solotask;

public interface StarClickListener {

    void onStarClick (int position, boolean checked);

}
