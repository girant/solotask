package am.artipd.solotask;

import android.graphics.Bitmap;

public class DataItemObject {
    String articleTitle, articleCategory, articleURL, newsId, apiUrl;
    Bitmap newsImage;
    Boolean isChecked;



    public DataItemObject(String articleTitle, String articleCategory, String articleURL, String id, String apiUrl, Boolean isChecked, Bitmap btmp) {
        this.articleTitle = articleTitle;
        this.articleCategory = articleCategory;
        this.apiUrl = apiUrl;
        this.articleURL = articleURL;
        this.isChecked = isChecked;
        this.newsId = id;
        newsImage = btmp;
    }
}
