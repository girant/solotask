package am.artipd.solotask;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashSet;
import java.util.Set;

import static am.artipd.solotask.GlobalVariables.EXTRA_API_URL;
import static am.artipd.solotask.GlobalVariables.EXTRA_ID;
import static am.artipd.solotask.GlobalVariables.EXTRA_NEWS_URL;
import static am.artipd.solotask.GlobalVariables.SET_API_URLS;
import static am.artipd.solotask.GlobalVariables.SET_NAME;
import static am.artipd.solotask.GlobalVariables.SPREFS_NAME;

public class OpenNewsPageActivity extends AppCompatActivity {
    private ImageView ivNewsImage;
    public Bitmap newsImageBitmap;
    FloatingActionButton fabAddToFavorites;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEdit;
    Set<String> prefsSet = new HashSet<>();
    Set<String> favSet = new HashSet<>();
    boolean isInNews = false;
    boolean pressedIdentificator = false;
    boolean stateChanged = false;
    SharedPreferences offlineNewsPreferemces;
    SharedPreferences.Editor offlineViewPrefsEditor;
    String newsId, url;
    WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_news_page);


        sharedPreferences = getSharedPreferences(SPREFS_NAME, MODE_PRIVATE);
        offlineNewsPreferemces = getSharedPreferences("offNews", MODE_PRIVATE);
        offlineViewPrefsEditor = offlineNewsPreferemces.edit();
        spEdit = sharedPreferences.edit();

        prefsSet = sharedPreferences.getStringSet(SET_NAME, new HashSet<>());
        favSet = sharedPreferences.getStringSet(SET_API_URLS, new HashSet<>());
        fabAddToFavorites = findViewById(R.id.fabAddToFavorites);


        webView = findViewById(R.id.webView);

        ivNewsImage = findViewById(R.id.ivNewsDetailsImage);

        Intent intent = getIntent();
        url = intent.getStringExtra(EXTRA_NEWS_URL);
        newsId = intent.getStringExtra(EXTRA_ID);
        String apiUrl = intent.getStringExtra(EXTRA_API_URL);
        Bitmap newsBmp = intent.getParcelableExtra("bitmap");
        isInNews = prefsSet.contains(newsId);
        if (offlineNewsPreferemces.getBoolean(newsId, false)){
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ONLY);
        }
        else {
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        }
        webView.loadUrl(url);



        if(isInNews){
            fabAddToFavorites.setImageResource(android.R.drawable.star_big_on);
            pressedIdentificator = true;
        }
        else{
            fabAddToFavorites.setImageResource(android.R.drawable.star_big_off);
            pressedIdentificator = false;
        }



        newsImageBitmap = newsBmp;
        ivNewsImage.setImageBitmap(newsImageBitmap);


        fabAddToFavorites.setOnClickListener(v->{
            if (pressedIdentificator){
                fabAddToFavorites.setImageResource(android.R.drawable.star_big_off);
                prefsSet.remove(newsId);
                favSet.remove(apiUrl);
                setResult(20);
            }
            else{
                fabAddToFavorites.setImageResource(android.R.drawable.star_big_on);
                prefsSet.add(newsId);
                favSet.add(apiUrl);
                setResult(21);
               // webView.saveWebArchive("nnn");
            }
            pressedIdentificator = !pressedIdentificator;
            spEdit.remove(SET_NAME);
            spEdit.remove(SET_API_URLS);
            spEdit.commit();

            spEdit.putStringSet(SET_NAME, prefsSet);
            spEdit.putStringSet(SET_API_URLS, favSet);

            spEdit.apply();
           // addedToFavList = true;
            stateChanged = !stateChanged;
            if (stateChanged){
               // setResult(11);
            }
            else setResult(10);

        });


        Log.d("SOLOAppOpenNewsPage", "News page open command getted");
        Log.d("SOLOAppOpenNewsPage", "URL is " + url);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_offline, menu);
        menu.getItem(0).setChecked(offlineNewsPreferemces.getBoolean(newsId, false));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.isChecked()){
            item.setChecked(false);
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        }
        else {
            item.setChecked(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        }
        webView.loadUrl(url);
        offlineViewPrefsEditor.putBoolean(newsId, item.isChecked()).apply();

        return super.onOptionsItemSelected(item);
    }
}
