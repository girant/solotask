package am.artipd.solotask;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestAndResponse {

    public static HttpURLConnection getConnection(String urlString)  {

        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(15000);
            connection.connect();

        } catch (IOException e) {
            Log.d("SOLOAppRequestAndRespon", e.getMessage());
        }
        return connection;
    }

    public static String getJsonString(HttpURLConnection connection){
        InputStream in;
        StringBuilder result = new StringBuilder();
        try {
            in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            Log.d("SOLOAppRequestAndRespon", "result from server: " + result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
