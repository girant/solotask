package am.artipd.solotask;

import android.widget.ImageView;

public interface ImageClickListener {

    void onImageClick(DataItemObject DIO, ImageView iv, int position);

}
