package am.artipd.solotask;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

public class CheckAndNotifyService extends Service {

    MyAsyncTasck myAsyncTasck;


    public CheckAndNotifyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myAsyncTasck = new MyAsyncTasck(this);

        Log.d("SOLOAppCheckService", "Service started");
        CountDownTimer timer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Log.d("SOLOAppCheckService", "Called timer onFinish");
                Log.d("SOLOAppCheckService", "Status is " + myAsyncTasck.getStatus());
                if (myAsyncTasck.getStatus().equals(AsyncTask.Status.FINISHED)){
                    myAsyncTasck = new MyAsyncTasck(getApplicationContext());
                    myAsyncTasck.execute();

                }
                else {
                    myAsyncTasck.cancel(true);
                    Log.d("SOLOAppCheckService", "Starting task again");
                    myAsyncTasck = new MyAsyncTasck(getApplicationContext());
                    myAsyncTasck.execute();
                }
                start();
            }
        }.start();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
