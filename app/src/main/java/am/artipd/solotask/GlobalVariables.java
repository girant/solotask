package am.artipd.solotask;

public class GlobalVariables {

    public static final String SPREFS_NAME = "APP_PREFS";
    public static final String FAV_PREFS_NAME = "FAV_PREFS";
    public static final String SET_NAME = "fav";
    public static final String SET_API_URLS = "apiurls";

    public static final String EXTRA_NEWS_URL = "newsUrl";
    public static final String EXTRA_IMAGE_URL = "imageUrl";
    public static final String EXTRA_ID  = "id";
    public static final String EXTRA_API_URL = "apiUrl";

    public static final String MAIN_URL_WITH_API_KEY = "https://content.guardianapis.com/search?api-key=3eecfebe-a4e5-4a48-a16b-724eb4dcab93&page=";
    public static final String API_KEY = "api-key=3eecfebe-a4e5-4a48-a16b-724eb4dcab93";


    public static String lastNewsDate;
    public static long lastNewsDateLong;
    public static boolean addedToFavList = false;
    public static final String NOT_CHANEL_ID = "SoloNewsChanel";
    public static final String NOT_CHANEL_NAME = "SoloNewsChanel";

    public static boolean viewMode;

    public static int inflaterLayoutId;

    public static final int NOT_ID = 2550525;
    public static boolean isActivityLaunched = true;
}
