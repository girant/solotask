package am.artipd.solotask;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static am.artipd.solotask.GlobalVariables.MAIN_URL_WITH_API_KEY;
import static am.artipd.solotask.GlobalVariables.NOT_CHANEL_ID;
import static am.artipd.solotask.GlobalVariables.NOT_CHANEL_NAME;
import static am.artipd.solotask.GlobalVariables.NOT_ID;
import static am.artipd.solotask.GlobalVariables.isActivityLaunched;
import static am.artipd.solotask.GlobalVariables.lastNewsDateLong;
import static am.artipd.solotask.RequestAndResponse.getConnection;
import static am.artipd.solotask.RequestAndResponse.getJsonString;

class MyAsyncTasck extends AsyncTask {
    String jsonString;
    JSONObject jsonObject;
    JSONObject jo;
    JSONArray jAray;
    String lastDate;
    long currLast;
    Context context;
    DataItemObject DIONEW;

    public MyAsyncTasck(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        DIONEW = null;
        jsonString = getJsonString(getConnection(MAIN_URL_WITH_API_KEY + 1));
        try {
            jsonObject = new JSONObject(jsonString);
            jo = jsonObject.getJSONObject("response");
            jAray = jo.getJSONArray("results");
            JSONObject jsonDIO = jAray.getJSONObject(0);
            Document doc = Jsoup.connect(jsonDIO.getString("webUrl")).timeout(0).get();
            Element image = doc.getElementsByClass("maxed responsive-img").first();
            String imageUrl;
            Bitmap imgBitmap;
            if (image!=null){
                imageUrl = image.absUrl("src");
                imgBitmap = Picasso.with(context).load(imageUrl).get();
            }
            else {
                imgBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.noimageavailb);
            }
            DIONEW = new DataItemObject(jsonDIO.getString("webTitle"),
                    "Category: " + jsonDIO.getString("sectionName"),
                    jsonDIO.getString("webUrl"), jsonDIO.getString("id"),
                    jsonDIO.getString("apiUrl"), false, imgBitmap);
            lastDate = jAray.getJSONObject(0).getString("webPublicationDate");
            DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = simpleDateFormat.parse(lastDate);
            currLast = date.getTime()/1000;

            Log.d("SOLOAppCheckService", String.valueOf(lastNewsDateLong));
            Log.d("SOLOAppCheckService", String.valueOf(currLast));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DIONEW;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Log.d("SOLOAppCheckService", "Before checking launched identificator: " + (isActivityLaunched));

        MainActivity mainActivity = MainActivity.getInstance();
        if(currLast>lastNewsDateLong){ //news are availb
            if (mainActivity!=null && isActivityLaunched){ //main activity is on the foreground
                Log.d("SOLOAppCheckService", "News are availb");
                mainActivity.refresh(DIONEW);

                Log.d("SOLOAppCheckService", "MainActivity is launched");

                Log.d("SOLOAppCheckService", String.valueOf(isActivityLaunched));

            }
            else {
                if (!isActivityLaunched){
                    //
                    // isActivityLaunched = false;
                    Log.d("SOLOAppCheckService", "The main window launched, news availb");
                    createNotify();
                }

            }
        }
    }

    public void createNotify(){
        createNotifChanel();
        Log.d("SOLOAppCheckService", "createNotifyWasCalled");
        Intent notifIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, notifIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Resources res = context.getApplicationContext().getResources();
        NotificationCompat.Builder notBuilder = new NotificationCompat.Builder(context.getApplicationContext(), NOT_CHANEL_ID)
                .setSmallIcon(R.drawable.noimageavailb)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher_background))
                .setContentTitle("News are availb")
                .setContentText("There are interesting news avails")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context.getApplicationContext());
        Log.d("SOLOAppCheckService", "Calling notify");
        notificationManager.notify(NOT_ID, notBuilder.build());
    }
    private void createNotifChanel(){

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = NOT_CHANEL_NAME;
            String description = "The guardians news";
            int inportance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOT_CHANEL_ID, name, inportance);
            channel.setDescription(description);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            Log.d("SOLOAppCheckService", "Notification chanel was created");
        }
    }
}
